# squirrels (development version)

# squirrels 0.0.1

# squirrels 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.

## Version 0.0.1

Bug fix, suppress one unused function

## Version 0.0.1.9000

Add the `check_squirrel_data_integrity()` function
