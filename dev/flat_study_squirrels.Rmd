---
title: "flat_study_squirrels.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
library(dplyr)
library(ggplot2)
library(magrittr)
library(tools)
library(readxl)
library(purrr)
library(stringr)

dput(study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray"))

read_excel(path = file.path("inst", "nyc_squirrels_17.xlsx"))
read_excel(path = file.path("inst", "nyc_squirrels_18.xlsx"))

possi_excel <- possibly(read_excel, otherwise = NULL)
possi_excel(path = file.path("inst", "nyc_squirrels_18.xlsx"))

data_nyc_squirrels_17oct <- possi_excel(path = system.file("nyc_squirrels_17.xlsx", package = "squirrels"))
data_nyc_squirrels_17oct


data_nyc_squirrels_18oct <- possi_excel(path = system.file("nyc_squirrels_18.xlsx", package = "squirrels"))
data_nyc_squirrels_18oct



possi_excel <- possibly(read_excel, otherwise = NULL)
  
  if ( isFALSE(dir.exists(system.file(package = "squirrels"))) ) {
    stop("The directory doesn't exist")
  }
  
  l <-
    list.files(
      path = system.file(package = "squirrels"),
      pattern = "nyc_squirrels_[0-9][0-9].xlsx",
      full.names = TRUE
    )
  
  if (isTRUE(identical(l, character(0)))) {
    stop("There is no file with data in the chosen directory")
  }
  
  list_file <- l %>% map(possi_excel)
  
  names(list_file) <- str_extract(l, "(?<=ls_)[0-9][0-9]")




```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)



```

# `get_message_fur_color()`: Get a message with a fur color

You can get a message with the fur color of interest with ´get_message_fur_color()´

```{r function-get_message_fur_color}

#' Get a message with the fur color of interest.
#'
#' @param primary_fur_color Character. The primary fur color of interest 
#' @importFrom glue glue
#'
#' @return Used for side effect. Outputs a message in the console
#' @export

get_message_fur_color <- function(primary_fur_color){
  message(glue("We will focus on {primary_fur_color} squirrels"))
}

```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Black"), 
    regexp = "We will focus on Black squirrels"
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Grey"), 
    regexp = "We will focus on Grey squirrels"
  )
})
```


# `study_activity()` study the activity of the squirrels by color 

This function takes a data frame, filter the data according to a primary fur color, summarizes their activities and creates a plot.

```{r function-study_activity}
#' Study the activities of the squirrels given a primary fur color
#'
#' @param df_squirrels_act Data frame. A dataset with the activities of the squirrels. This dataset mush have at least these 4 columns: "age", "primary_fur_color", "activity", "counts".
#' @param col_primary_fur_color Character. The color of the primary fur color of interest. Only the squirrels with this primary fur color will be considered in the analysis.
#' 
#' @importFrom dplyr filter
#' @importFrom ggplot2 ggplot aes geom_col scale_fill_manual labs
#' @importFrom magrittr %>% 
#'
#' @return A list of two named elements. The first one is the filtered table. The second one is the ggplot.
#' @export
#'
#' @examples
study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  if(isFALSE(is.data.frame(df_squirrels_act))) {
    stop("df_squirrels_act is not a data frame")
  }
  
  check_squirrel_data_integrity(df_squirrels_act)
  
  if(isFALSE(is.character(col_primary_fur_color))) {
    stop("col_primary_fur_color is not a character vector")
  }
  
  
  table <- df_squirrels_act %>% 
    filter(primary_fur_color == col_primary_fur_color)
    
  graph <- table %>% 
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))
  
  return(list(table = table, graph = graph))
}
```
  
```{r example-study_activity}
data(data_act_squirrels)
study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")
```
  
```{r tests-study_activity}
test_that("study_activity works", {
  data(data_act_squirrels)
  
  res <- study_activity(
      df_squirrels_act = data_act_squirrels,
      col_primary_fur_color = "Gray")
  
  expect_equal(
    object = res$table,
    expected = structure(
      list(
        age = c("Adult", "Adult", "Adult", "Adult", "Adult"),
        primary_fur_color = c("Gray", "Gray", "Gray", "Gray", "Gray"),
        activity = c("running", "chasing", "climbing", "eating", "foraging"),
        counts = c(513, 195, 456, 524, 1046)
      ),
      row.names = c(NA, -5L),
      spec = structure(list(
        cols = list(
          age = structure(list(), class = c("collector_character",
                                            "collector")),
          primary_fur_color = structure(list(), class = c("collector_character",
                                                          "collector")),
          activity = structure(list(), class = c("collector_character",
                                                 "collector")),
          counts = structure(list(), class = c("collector_double",
                                               "collector"))
        ),
        default = structure(list(), class = c("collector_guess",
                                              "collector")),
        delim = ","
      ), class = "col_spec"),
      class = c("spec_tbl_df", "tbl_df", "tbl", "data.frame")
    )
  )
  
  expect_true(
    object = inherits(res$graph, "ggplot")
  )
  
  expect_true(
    object = inherits(res$table, "data.frame")
    )
})

test_that("study_activity fails", {
  
  expect_error(
    object = study_activity(
      df_squirrels_act = 1,
      col_primary_fur_color = "Gray"))
  
  expect_error(
    object = study_activity(
      df_squirrels_act = data_act_squirrels,
      col_primary_fur_color = 1))

}
)
```
  
# `save_as_csv()`: save as csv the data passed as argument
  
This function takes a data frame and a character vector and save it as a csv file using the character vector as the path. All the parameters of the `write.csv2` function can be passed to `save_as_csv()`
    
```{r function-save_as_csv}

#' Save a data set in a given path
#'
#'
#'
#' @param file_squirrel Data.frame. The dataset that will be saved in csv
#' @param a_path Character. The path of the csv file
#' @param ... arguments to `write.csv2`
#'
#' @importFrom tools file_ext
#' @importFrom utils write.csv2
#' @return Character. The path of the created file.
#' @export
#'
#' @examples
save_as_csv <- function(file_squirrel, a_path, ...) {
  if (isFALSE(inherits(file_squirrel, "data.frame"))) {
    stop("file_squirrel is not a data frame")
  }
  
  if (isTRUE(nrow(file_squirrel) == 0)) {
    stop("file_squirrel has no observation (0 row)")
  }
  
  if (isFALSE(file_ext(a_path) == "csv")) {
    stop("the file extension is not csv")
  }
  
  if (isFALSE(dir.exists(dirname(a_path)))) {
    stop("the folder doesn't exist")
  }
  
  write.csv2(file_squirrel, file = a_path, row.names = FALSE, ...)
  
  normalizePath(a_path)
  
}
```
  
```{r example-save_as_csv}
my_temp_dir <- tempfile(pattern = "savecsv")
dir.create(my_temp_dir)

data(data_act_squirrels)
data_reprex <- study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")

save_as_csv(file_squirrel = data_reprex$table, 
            a_path = file.path(my_temp_dir, "myfile.csv"),
            fileEncoding = "UTF-8")

unlink(my_temp_dir, recursive = TRUE)
```
  
```{r tests-save_as_csv}

test_that("save_as_csv works", {
  
  my_temp_dir <- tempfile(pattern = "savecsv")
  dir.create(my_temp_dir)
  
  data(iris)
  
  out <- save_as_csv(file_squirrel = iris, 
                    a_path = file.path(my_temp_dir, "out.csv"))
  
  expect_type(out, "character")
  expect_true(file.exists(out))
  expect_equal(iris, 
               read.csv2(file.path(my_temp_dir, "out.csv"), stringsAsFactors = TRUE))
  expect_true(out == file.path(my_temp_dir, "out.csv"))
  
  unlink(my_temp_dir, recursive = TRUE)
  
})


test_that("save_as_csv fails", {
  
  my_temp_dir <- tempfile(pattern = "savecsv")
  dir.create(my_temp_dir)
  
  data(iris)
  
  expect_error(save_as_csv(file_squirrel = iris, 
                    a_path = file.path(my_temp_dir, "toto/out.csv")))
  
    expect_error(save_as_csv(file_squirrel = 1, 
                    a_path = file.path(my_temp_dir, "out.csv")))
    
    expect_error(save_as_csv(file_squirrel = iris, 
                    a_path = file.path(my_temp_dir, "out")))
    
    expect_error(save_as_csv(file_squirrel = iris[FALSE,], 
                    a_path = file.path(my_temp_dir, "out")))
})
```

# `read_data_day_squirrels()` : import .xlsx data in inst folder
    
This function import .xlsx data saved in the `inst` folder and return `NULL` if the file cannot be imported

```{r function-read_data_day_squirrels}
#' Safely import .xlsx data
#' 
#' This function import .xlsx data saved in the `inst` folder and return `NULL` if the file cannot be imported
#' 
#' @param dir_data character. Path of the excel files
#' 
#' @importFrom readxl read_excel
#' @importFrom purrr possibly
#' @importFrom purrr map
#' @importFrom stringr str_extract
#' 
#' @return list. Containing the data or NULL as element
#' 
#' @export
#' 
read_data_day_squirrels <- function(dir_data) {
  possi_excel <- possibly(read_excel, otherwise = NULL)
  
  if ( isFALSE(dir.exists(dir_data)) ) {
    stop("The directory doesn't exist")
  }
  
  l <-
    list.files(
      path = dir_data,
      pattern = "nyc_squirrels_[0-9][0-9].xlsx",
      full.names = TRUE
    )
  
  if (isTRUE(identical(l, character(0)))) {
    stop("There is no file with data in the chosen directory")
  }
  
  list_file <- l %>% map(possi_excel)
  
  names(list_file) <- str_extract(l, str_extract(l, "(?<=ls_)[0-9][0-9]"))
  return(list_file)
}
```
  
```{r example-read_data_day_squirrels}
out_data <- read_data_day_squirrels(dir_data = system.file(package = "squirrels"))
out_data

# number of rows of each imported data.set i.e., of each element of the list
out_data %>%
  purrr::map(nrow)

# print only suceesfully imported data.set
out_data %>%
  purrr::compact()

```
  
```{r tests-read_data_day_squirrels}
test_that("read_data_day_squirrels works", {
  out <- read_data_day_squirrels(dir_data = system.file(package = "squirrels"))
  expect_equal(
    object = out$"17",
    expected = read_excel(path = file.path("/home/rstudio/squirrels/inst/nyc_squirrels_17.xlsx")))
})


test_that("read_data_day_squirrels fails", {
  expect_error(
    object = read_data_day_squirrels(
    dir_data = "toto"),
    regexp = "The directory doesn't exist")
  expect_error(
    object = read_data_day_squirrels(
    dir_data = system.file("data", package = "squirrels")),
    regexp = "There is no file with data")
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", 
               vignette_name = "Study the squirrels")
```
