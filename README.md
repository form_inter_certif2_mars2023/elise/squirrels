
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to study NYC squirrels from Central Park. The
data were collected by a non profit organization. This package is
created for the N2 training.

## Installation

You can install the development version of squirrels like so:

``` r
  
remotes::install_local(path = "path/to/squirrels_0.0.0.9000.tar.gz", 
                        build_vignettes = TRUE)
```

## Example

This is a basic example which shows you how the function
`get_message_fur_color` works:

``` r
library(squirrels)
## basic example code
get_message_fur_color(primary_fur_color = "Cinnamon")
#> We will focus on Cinnamon squirrels
```

## Code of Conduct

Please note that the squirrels project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
