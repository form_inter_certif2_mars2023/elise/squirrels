# WARNING - Generated by {fusen} from /dev/flat_study_squirrels.Rmd: do not edit by hand


test_that("save_as_csv works", {
  
  my_temp_dir <- tempfile(pattern = "savecsv")
  dir.create(my_temp_dir)
  
  data(iris)
  
  out <- save_as_csv(file_squirrel = iris, 
                    a_path = file.path(my_temp_dir, "out.csv"))
  
  expect_type(out, "character")
  expect_true(file.exists(out))
  expect_equal(iris, 
               read.csv2(file.path(my_temp_dir, "out.csv"), stringsAsFactors = TRUE))
  expect_true(out == file.path(my_temp_dir, "out.csv"))
  
  unlink(my_temp_dir, recursive = TRUE)
  
})


test_that("save_as_csv fails", {
  
  my_temp_dir <- tempfile(pattern = "savecsv")
  dir.create(my_temp_dir)
  
  data(iris)
  
  expect_error(save_as_csv(file_squirrel = iris, 
                    a_path = file.path(my_temp_dir, "toto/out.csv")))
  
    expect_error(save_as_csv(file_squirrel = 1, 
                    a_path = file.path(my_temp_dir, "out.csv")))
    
    expect_error(save_as_csv(file_squirrel = iris, 
                    a_path = file.path(my_temp_dir, "out")))
    
    expect_error(save_as_csv(file_squirrel = iris[FALSE,], 
                    a_path = file.path(my_temp_dir, "out")))
})
